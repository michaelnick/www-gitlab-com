---
layout: markdown_page
title: "All Remote Benefits"
---

## On this page
{:.no_toc}

- TOC
{:toc}

On this page, we're highlighting benefits of operating an all-remote company for employers, employees, and the world. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Xw-31PZkHOo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [video](https://www.youtube.com/watch?v=Xw-31PZkHOo) above, GitLab Director of Global People Operations [Carol Teskey](https://gitlab.com/cteskey) shares her view on the many benefits of all-remote, and the competitive advantages that come along with it.

### All-Remote Advantages

Reimagining how one's day can be structured, and how that can easily vary from one 24-hour period to the next, is empowering. The freedom and flexibility that comes with all-remote enables employees to view work in an entirely new light. 

Rather than forcing one to build their life about a predefined daily schedule that involves an unavoidable commute, all-remote shifts that responsibility back to the individual. 

A number of studies from the likes of [Google](https://www.forbes.com/sites/abdullahimuhammed/2019/05/18/5-important-takeaways-from-googles-two-year-study-of-remote-work/#1a536957439a), [Buffer](https://buffer.com/state-of-remote-work-2019), [FlexJobs](https://www.techrepublic.com/article/why-remote-work-has-grown-by-159-since-2005/), and [IWG](http://assets.regus.com/pdfs/iwg-workplace-survey/iwg-workplace-survey-2019.pdf) show that driven individuals who place a high degree of value on autonomy and flexibility can experience new levels of joy and productivity in an all-remote environment. 

#### For employees

1. You have more [flexibility](http://shedoesdatathings.com/post/1-year-at-gitlab/) in your daily life (for [kids](/2019/07/25/balancing-career-and-baby/), parents, friends, groceries, sports, deliveries).
1. No more time, [stress](https://www.forbes.com/sites/markeghrari/2016/01/21/a-long-commute-could-be-the-last-thing-your-marriage-needs/#5baf10f04245), or money wasted on a [commute](https://www.inc.com/business-insider/study-reveals-commute-time-impacts-job-satisfaction.html) (subway and bus fees, gas, car maintenance, tolls, etc.).
1. Reduced [interruption stress](/2018/05/17/eliminating-distractions-and-getting-things-done/) and increased [productivity](https://www.inc.com/brian-de-haaff/3-ways-remote-workers-outperform-office-workers.html).
1. Ability to [travel to other places](/2017/01/31/around-the-world-in-6-releases/) without taking vacation (family, fun, etc.).
1. Freedom to relocate, be [location independent](/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/), or even [travel with other remote professionals](/company/culture/all-remote/resources/#organizations-for-traveling-remote-work).
1. Less exposure to germs from sick coworkers.
1. It can be [easier to communicate](/company/culture/all-remote/informal-communication/) with difficult colleagues remotely, [reducing distractions](/2018/03/15/working-at-gitlab-affects-my-life/) from interpersonal drama or office politics.
1. You can [set up and decorate your office or workspace](https://thriveglobal.com/stories/how-remote-work-can-reduce-stress-and-revitalize-your-mindset/) in whatever way [works best for you](/2019/08/01/working-remotely-with-children-at-home/).
1. You can choose your working hours based on when you're most productive.
1. You have the opportunity to [meet and work with people from many locations around the world](/handbook/incentives/#visiting-grant), widening one's view of the world and creating opportunities to learn about new cultures.
1. [Onboarding may be less stressful socially](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
1. Eating at home is better (sometimes) and cheaper.
1. Taxes can be cheaper in some countries.
1. Work clothes [are not required](/2019/07/09/tips-for-working-from-home-remote-work/).

From family time to travel plans, there are [many examples and stories](/company/culture/all-remote/stories/) of how remote work has impacted the lives of GitLab team members around the world.

> **“The flexibility makes family life exponentially easier, which reduces stress and makes you more productive and motivated. You can’t put a dollar value on it – it’s priceless.”** - Haydn, Regional Sales Director, GitLab  

#### For your organization

Limiting your company's recruiting pipeline to a certain geographic region, or sourcing employees who are able and willing to relocate, is a competitive disadvantage. 

Not only does this create a less [inclusive](/company/culture/inclusion/) hiring process which reaches a less diverse set of candidates, it forces your organization to compete primarily on the basis of [salary](https://hired.com/page/state-of-salaries). 

1. You're able to hire great people [no matter where they live](/jobs/faq/#country-hiring-guidelines).
1. Employees are [more productive with fewer distractions](/2018/05/11/day-in-life-of-remote-sdr/).
1. [Increased savings on office costs](https://globalworkplaceanalytics.com/the-remote-work-roi-calculator-v0-95), [compensation](/2019/02/28/why-we-pay-local-rates/) (due to hiring in lower-cost regions).
1. All-remote naturally attracts [self-motivated people](/2019/03/28/what-its-like-to-interview-at-gitlab/).
1. It's easier to quickly grow your company.
1. Employees are [increasingly](https://www.iofficecorp.com/blog/workplace-design-statistics) expecting remote work options from their employers.
7. Companies often experience [lower employee turnover](https://www.owllabs.com/blog/remote-work-statistics) and higher morale with remote work.
1. You have [fewer meetings](/company/culture/all-remote/meetings/) and more focus on results and [output of great work](/handbook/values/#results).
1. You don't have to pay to relocate someone to join your team.
1. With employees located all over the world [working asynchronously](/2019/02/27/remote-enables-innovation/), contributions can continue even when one time zone's working day is over.
1. There's also business continuity in the case of local disturbances or natural disasters (e.g. political or weather-related events).
1. Greater flexibility [can mean greater diversity](https://business.linkedin.com/content/dam/me/business/en-us/talent-solutions/resources/pdfs/global-talent-trends-2019.pdf) in your organization. 

#### For the world

[Research from the University of New Hampshire](https://carsey.unh.edu/publication/rural-depopulation) has found that "35% of rural counties in the United States are experiencing protracted and significant population loss." Speaking to shrinking towns across Europe, [a 2016 report from the European Parliamentary Research Service](http://www.europarl.europa.eu/thinktank/en/document.html?reference=EPRS_BRI(2016)586632) notes that "younger members of society prefer to migrate to more economically vibrant regions and cities in search of better job prospects as, in most of these territories, professional opportunities remain limited and confined to specific fields (e.g. agriculture and tourism)." 

We believe all-remote has the power to pause, and perhaps even reverse, these trends of depopulation.

Working remotely gives each person the autonomy to serve in a place that matters to them – a place that has shaped them – contributing significantly to the wellbeing of a population that may be at risk of losing its foundation, should talent continue to flee to the usual job centers.

1. With no commuting employees and no office buildings or campuses, all-remote companies have a smaller environmental footprint (except when they host regular companywide summits or gatherings that require significant amounts of air travel).
1. There's evidence that [remote work can reduce the effects of urban crowding](https://qz.com/work/1641664/remote-workers-are-the-solution-to-urban-crowding/) for many cities around the world. Some states and countries are even [offering incentives](http://fortune.com/2019/06/22/google-housing-plan-bay-area/) to encourage remote work. Here are [13 examples](https://www.bankrate.com/personal-finance/smart-money/places-that-will-pay-you-to-move/#slide=1) around the world.
1. For global companies, bringing better-paying jobs to low-cost regions has positive economic impacts and works to reverse the [trends](http://www.europarl.europa.eu/thinktank/en/document.html?reference=EPRS_BRI(2016)586632) of [depopulation in rural regions](https://carsey.unh.edu/publication/rural-depopulation).


----

Return to the main [all-remote page](/company/culture/all-remote/).
