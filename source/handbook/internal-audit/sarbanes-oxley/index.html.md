---
layout: markdown_page
title: "Sarbanes-Oxley (SOX) Compliance"
---


## On this page
{:.no_toc}

- TOC
{:toc}

# Sarbanes-Oxley Act of 2002
[ Sarbanes Oxley Act 2002 ](https://en.wikipedia.org/wiki/Sarbanes%E2%80%93Oxley_Act)is a federal law that established auditing and financial regulations for financial reporting of public companies. This law was passed to increase transparency in  financial reporting by corporations and to require a formalized system of checks and balances in each company, thereby helping protect investors from fraudulent financial reporting. SOX applies to all publicly traded companies in the United States as well as wholly-owned subsidiaries and foreign companies that are publicly traded and do business in the United States. 

In order to build the confidence of the investors, the SOX regulations require the following:

*	CEOs and CFOs are directly responsible for the accuracy, documentation, and submission of all financial reports as well as the internal control structure to the [ SEC ](https://www.sec.gov/). 
*	Internal Control Report must state that management is responsible for an adequate internal control structure for their financial records. Any shortcomings must be reported up the chain as quickly as possible for transparency.
*	Companies should develop and implement a comprehensive data security strategy that protects and secures all financial data stored and utilized during normal operations.
*	Companies must maintain and provide documentation proving they are compliant and that they are continuously monitoring and measuring SOX compliance objectives.
*	External auditor must independently assess and certify the adequacy of controls over all known risks for the financial reporting. 

Formal penalties for non-compliance with SOX can include fines, removal from listings on public stock exchanges and invalidation of [ D&O ](https://en.wikipedia.org/wiki/Directors_and_officers_liability_insurance) policies. Under the Act, CEOs and CFOs who wilfully submit an incorrect certification to a SOX compliance audit can face fines and/or imprisonment.


<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/wZ8xDBgMat8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

# SOX Compliance Roadmap for GitLab 

As GitLab is planning to go public in November 2020, it is important that we prepare to comply with SOX ahead of listing, as the consequences of non-compliance can be severe.  Below are some of the important requirements we will need to adhere to: 
<div align="center">
    <img src="/images/handbook/internal-audit/sarbanes-oxley_section.jpg" align="center" height="400" width="600" >
</div>    



Points to note:
*	In the first year, an “exemption” is allowed for a new public company on [ 10-K ](https://en.wikipedia.org/wiki/Form_10-K) certification under  [ 404(a) ](https://www.cpajournal.com/2016/02/01/auditors-need-know-sox-section-404a-reports/) , therefore, 10-K - 404(a) certification can be submitted for GitLab from FY 2022 (2nd year) onwards. 
*	Additionally, the external audit opinion on internal controls, which is required under [ sec 404(b) ](https://www.cpajournal.com/2016/02/01/auditors-need-know-sox-section-404a-reports/), is temporarily exempt for [ Emerging Growth Companies ](https://www.sec.gov/smallbusiness/goingpublic/EGC) (EGCs) for the first five years. While GitLab currently qualifies as an EGC, it is to be noted that this exemption is withdrawn if the criteria for EGCs are not satisfied in any year. 

Planned timelines for SOX Implementation:

<div align="center">
    <img src="/images/handbook/internal-audit/sarbanes-oxley_timeline.png" align="center" height="400" width="600" >
</div>

# SOX Program Management Office ([PMO](https://en.wikipedia.org/wiki/Project_management_office))

The SOX PMO, a division of internal audit department is tasked with the compliance provisions of the Sarbanes-Oxley Act of 2002 (“SOX”). The below [RACI ](https://en.m.wikipedia.org/wiki/Responsibility_assignment_matrix)table is used for clarifying, defining roles and responsibilities in cross-functional or departmental projects and processes.

| **#** | **Activity** |**Responsibility (R)**|**Accountability (A)** |**Consulted (C)**|**Informed (I)**|
| ------ | ------ |------ |------ |------ |------ |
| 1| SOX Scoping and Risk Assessment |PMO|PAO |PO|KSH|
| 2| RCM Preparation and Reviews |PMO|PAO |PO|KSH|
| 3| RCM Confirmation and Sign-off |PO|PAO |PMO|CFO|
| 4| Control Change Updates |PMO|PAO |PO|PO|
| 5| Remediation of Gaps |PO|PAO |PMO|CFO|
|6| Process Flowcharts  |PMO|PAO |PO|PO|
| 7| SOX 404 Management Testing |PMO|PAO |PO|CFO, KSH|
| 8| SOX 302 Certification |PMO|CEO, CFO|PAO|KSH|
| 9| SOX 404 Certification |PMO|CEO, CFO|PAO|KSH|
| 10| 10-K filing |PAO|CEO, CFO|PAO|KSH|
| 11 |10-Q filing |PAO|CEO, CFO|PMO|KSH|

*  CEO - Chief Executive Officer
*  CFO - Chief Financial Officer
*  PAO - Principal Accounting Officer
*  KSH - Key Stakeholders
*  PMO - Internal Audit, SOX PMO
*  PO  - Process Owner

# Sarbanes-Oxley Section 404 Management Testing Plan 

**Objective**

The objective of this document is to summarize management’s approach to plan, organize, execute, document and support its assessment of the effectiveness of GitLab and its subsidiaries’internal control over financial reporting. Specifically, this document will address the following:


*  Nature, extent and timing of evaluation procedures
*  Techniques used to evaluate the design and operating effectiveness of controls
*  Documentation requirements with respect to the evaluation
*  Frequency of evaluation
*  Reporting and tracking evaluation results
*  Reporting and tracking of the evaluation status

**Management's Responsibilities** 

Under the SEC’s rules implementing the requirement of Section 404 of the Sarbanes Oxley Act of 2002, each annual report must include management’s report on internal control over financial reporting that contains the following elements:

*  A statement of management responsibility for establishing and maintaining adequate internal control over financial reporting for the company;
*  A statement identifying the framework used by management to conduct the required evaluation of the effectiveness of the company’s internal control over financial reporting;
*  Management’s assessment of the effectiveness of the company’s internal control over financial reporting as of the end of GitLab’s most recent fiscal year, including a statement as to whether or not the company’s internal control over financial reporting is effective. The assessment must include disclosure of any “material weaknesses” in the company’s internal control over financial reporting identified by management. Management is not permitted to conclude that the company’s internal control over financial reporting is effective if there are one or more material weaknesses in the company’s internal control over financial reporting; and
*  A statement that the registered public accounting firm that audited the financial statements included in the annual report has issued an attestation report on management’s assessment of GitLab’s internal control over financial reporting.

The remainder of this document summarizes the scope and approach of management’s assessment of the effectiveness of the company’s internal control over financial reporting (third bullet above).


**COSO Framework**

GitLab has adopted the COSO framework as the criteria for evaluating the effectiveness of the company’s internal control over financial reporting. The COSO framework includes the following components:

*  Control Environment
*  Risk Assessment
*  Control Activities
*  Information and Communication
*  Monitoring

Management’s overall assessment must take into consideration all five components of the COSO framework. The decision as to whether an organization’s control structure is operating satisfactorily or not is ultimately one of judgment, dependent upon the relative significance placed on any given component of the control framework.

**Project Team and Timing**

Senior Internal Audit Manager has been identified as the project leader and will assume the responsibility for providing the overall direction to the project teams and for communicating the project status to management. An in-house internal audit team and a third party consulting firm, as required, will assist the project leader in this effort.
Significant milestones relating to the management evaluation of internal controls over financial reporting and estimated timing is defined [here](https://about.gitlab.com/handbook/internal-audit/sarbanes-oxley/#sox-compliance-roadmap-for-gitlab).
As testing begins, a detailed timeline for completing the testing of internal controls by process will be prepared.Project leader will report project status and results to the management on a monthly basis.

**Approach to evaluation**

This document lays the framework for testing the effectiveness of controls over all relevant assertions related to all significant accounts and disclosures in the financial statements. 

Controls that are significant generally include:

*  Controls over initiating, authorizing, recording, processing, and reporting significant accounts and disclosures and related assertions embodied in the financial statements.
*  Controls over the selection and application of accounting policies that are in conformity with generally accepted accounting principles.
*  Antifraud programs and controls
*  Controls, including information technology general controls, on which other significant controls are dependent.
*  Controls over significant non-routine and nonsystematic transactions, such as accounts involving judgments and estimates.
*  Company level controls, including:
   - 	The control environment and
   - 	Controls over the period-end financial reporting process, including controls over procedures used to enter transaction totals into the general ledger; to initiate, authorize, record and process journal entries in the general ledger; and to record recurring and nonrecurring adjustments to the financial statements (for example, consolidating adjustments, report combinations, and reclassifications).
*  To determine the controls to be included in the testing plan, management will also consider the following matters:
*  The nature of the control (e.g., its subjectivity).
*  The likelihood of material misstatement that would result from failure of the control.
*  The risk that the control might not be operating effectively, which might be assessed by considering the following:
    -  Whether there have been changes in the volume or nature of transactions that might adversely affect control design or operating effectiveness.
    -  Whether there have been changes in the design of controls.
    -  The degree to which the control relies on the effectiveness of other controls.
     -  Whether there have been changes in key personnel who perform the control or monitor its performance.
     -  Whether the control relies on performance by an individual or is automated.
     -  The complexity of the control.
*  Anti-fraud controls, general controls, safeguarding controls, authorization controls and the maintenance of records.
*  The significance of the control in achieving the objectives of the control criteria and whether more than one control achieves a particular objective.

Management’s approach to its assessment of the effectiveness of the company’s internal control over financial reporting is organized in two phases: (1) assessing the entity-wide control framework and (2) assessing process level controls.

**Assessing the entity-wide control framwork**

Management will document the company’s existing entity-wide/corporate control framework and identify potential gaps between the company’s framework and the COSO control framework. The documentation and analysis of the framework will be focused on the entity-wide/corporate internal control over financial reporting, particularly relating to the control environment, risk assessment and monitoring components of COSO. In order to apply and/or assess the components of COSO as they relate to internal control over financial reporting, management must gain an understanding as to the criteria for rating them. In other words, how does the company determine if the control environment, risk assessment process, etc. are satisfactory? Included in Appendix A are some of the key control factors of each of the five COSO components relating to internal control over financial reporting.

**Assessing process level controls**

Over the past few months, SOX PMO have prepared risk and control matrices in consulation with process owners. In addition to documenting GitLab’s business processes, these risk and control matrices identify the controls over relevant assertions related to all significant accounts and disclosures in the financial statements. These risk and control matrices will be the basis for assessing process level controls. The matrices are to be reviewed and approved by the business process owners and the project leader prior to executing control assessment procedures. 

Assessing process level controls will include an evaluation of control design and an evaluation of control effectiveness.

**Evaluating design effectiveness of controls**

Design effectiveness refers to whether a control is suitably designed to prevent or detect material misstatements in financial statements. It involves consideration of the financial reporting objectives that the control is meant to achieve and whether it will achieve them. When evaluating control design, management should consider the following:

*  Whether the control achieves the financial reporting objective.
*  How the control is performed.
*  The frequency with which the control is performed.
*  The competence and experience of individuals involved in the process and executing the control.
*  The nature and size of misstatement that the control is likely to detect.
*  Follow-up activity on exceptions identified by the control.

Consideration should be given to each significant control in a group of controls that function together to achieve a control objective. While it is expected that the majority of controls identified will be tested, insignificant controls do not need to be tested if there is another control that will adequately cover the objective.

It is the responsibility of line management to evaluate the design effectiveness of controls in their respective area. Line management will use the recently completed process, risk and control documentation to identify whether there are financial reporting risks not mitigated by controls, e.g., are the controls designed effectively. The risk and control matrices, together with a listing of control gaps or deficiencies, will serve as evidence of this evaluation. If the design of a particular control is deemed to be inadequate or a control gap is identified, business process owners will implement additional controls or changes in the design of existing controls. To maintain management’s evaluation of design effectiveness current, line management will review process, risk and control documentation at quarterly intervals to identify any new or changed risks and highlight the relevant controls that have been implemented to mitigate these risks.

**Evaluating operating effectiveness of controls**

Operating effectiveness refers to whether the control is functioning as designed. During the evaluation of operating effectiveness, management gathers evidence regarding how the control was applied, the consistency with which it was applied, and by whom it was applied.
SOX PMO will execute testing procedures, approved by management, to support management’s evaluation of operating effectiveness of controls for in-scope routine processes.
In developing the plan for evaluating operating effectiveness of controls, management will consider the following:

*  Selection of Test Type and Control Categories
*  Extent of Testing (Sample Sizes)
*  Timing of Testing
*  Documentation of Test Results

Prior to performing test work on the operating effectiveness of internal control over financial reporting, audit programs will be prepared. The audit program will set out the nature, timing and extent of the procedures to be performed and will serve as a set of instructions to those performing the work. Audit programs will include:

*  Description of the audit program objectives.
*  A description and record of the nature, timing and extent of evaluation and other procedures performed to support the audit program objectives.
*  An indication as to who performed the procedures and when they were performed.
*  A conclusion as to whether the procedures performed accomplished the objectives of the audit program.

**Selection of Test Type and Control Categories**

There are a number of techniques that may be used to obtain evidence about the effectiveness of the operation of controls. These techniques include observation, inquiry, inspection and re-performance. There are additional techniques that, when combined with the previously listed techniques may be used to gain sufficient and appropriate evidence related to the operating effectiveness of a control. These techniques include knowledge assessment, corroborative inquiry and system query. See description of testing techniques included in Appendix B.

To determine the appropriate testing technique, it is first necessary to categorize controls into a control type. Controls can generally be categorized into the following:


*  	Authorization
*  	Exception/edit reports
*  	Interface/conversion controls
*  	Key performance indicators
*  	Management review
*  	Reconciliation
*  	Segregation of duties
*  	System access
*  	System configuration/account mapping controls.

Selection of an appropriate control category is an important step as the category selected is directly linked to the types of test procedures. General testing steps for determining control effectiveness for each control type are outlined in Appendix C. For controls not falling into the control categories listed above, testing procedures will be determined based on the distinct nature of the control.

**Extent of Testing (Sample Sizes)**

It is necessary to test controls to the extent deemed necessary for management to be satisfied that the results of the test provide conclusive evidence for management to support the assertion that the control is operating effectively. Once it is decided which technique or combination of techniques to use, the number of items to test in determining whether the control is operating effectively and consistently depends on how the control was applied, the consistency with which it was applied, and by whom the control was performed.

The first step in determining sample size is determining if the control is manual or automated (i.e., system controls). Manually applied controls are prone to random failures, whereas automated controls should be reliable and, as long as the computer systems are working effectively, will tend to operate consistently.

The nature of the control (i.e., manual or automated) will be documented in the risk and control matrices. The following baselines will be used in making sample size judgments for manual and automated controls:

When testing monitoring or manual controls, the sample size will depend on the frequency at which the control occurs. Testing standards are as follows:

| Frequency of Control | Baseline Sample Size |
| ------ | ------ |
| Annual | 1 |
| Semi-annual | 1 |
| Quarterly | 2 |
| Monthly | 3 |
| Fortnight | 5 |
| Weekly | 5 | 
| Daily / Recurring (multiple times per day)| 25 | 

In situations where an automated or IT control exists and is applied to every transaction, testing will generally be minimal as management will have tested general computer controls to be satisfied that they are functioning appropriately. Therefore, system query will often be the most appropriate testing technique. In this technique, one query as a test is appropriate for an IT system that would be expected to operate consistently in a well-controlled environment. A well-controlled environment is one where the specific configuration, interfaces and system access are appropriately designed and subject to appropriate change control procedures. Observation should also be made to ascertain whether there is any violation to the security access such as sharing of passwords.

Samples should be selected randomly to reflect an appropriate representation of the population. The specific sources and populations used for making sample selections will vary from control to control. Determining the appropriate source and population is a matter of judgment and should consider the following:

*  	Pervasiveness of the control and variety of instances in which the control is applied or by whom the control is applied.
*  	Changes in key personnel who perform the control or monitor its performance.
*  	Consistency with which the control is applied, i.e., sample sources and populations should cover an amount of time that will allow management to conclude on the consistency with which the control was applied.
*  	Quantitative measures for selecting high value items.

The above guidance on extent of testing applies in situations where there is a population to sample from. Controls may also exist where testing techniques and sample sizes may not be applicable. For example:

*  In situations where the knowledge assessment technique is used, extent to testing should be sufficient to provide comfort regarding the knowledge of that one individual.
*  In situations where corroborative inquiry is used, the extent of testing should be sufficient to confirm consistency in application of the control.

Rationale for determining sample selection sources and populations will be documented in the working papers, as appropriate.

**Timing of Testing**

Test of controls should be performed over a period of time that is adequate to determine whether, as of the date specified in the assertion, the controls necessary for achieving the objectives of the control criteria are operating effectively. The period of time over which tests of controls should be performed is a matter of management judgment.

If controls are to be tested over a period of time such as at an interim date using techniques other than knowledge assessment or corroborative inquiry (i.e., inspection, observation, re-performance), management should consider what additional evidence concerning the operation of the control should be obtained for the remaining period. 

Evidence should be obtained about the nature and extent of any significant changes in internal control that occur subsequent to the previous or interim date through, for example, inquiry or observation. In addition, sufficient evidence should be obtained about the operating effectiveness of such controls since the previous or interim date, for example, by obtaining evidence about the operating effectiveness of the company’s monitoring of controls.

Prior to the date specified in the assertion, management may change the company’s controls to make them more effective or efficient, or to address control deficiencies. In these circumstances, controls that have been superseded may not need to be considered. For example, if management asserts that the new controls achieve the related objectives of the control criteria and have been in effect for a sufficient period and there is sufficient time to permit the testing of the new design and operating effectiveness, tests may not need to be performed on the design and operating effectiveness of the superseded controls, except to the extent of communicating identified significant deficiencies in controls that might have been identified in an interim period.

Management will prepare a detailed timeline for evaluation of control design and control effectiveness, as well as timelines to address control deficiencies and update control documentation.

**Documentation of Test Results**

In evaluating and developing the assessment of internal control over financial reporting, evidential matter needs to be maintained to provide reasonable support in the review of management’s assessment and attestation conducted by the independent auditor. This evidential matter must provide sufficient documentation to enable the external auditor to conclude that there is a reasonable basis for the assertion on internal control over financial reporting that will be made by management. 

Working papers will be prepared and crossed referenced to enable reviewers and external auditors to easily locate the documentation that supports the conclusion reached on the assessment of the selected internal controls over financial reporting. Working papers will include sufficient documentation to re-perform control testing (i.e., copies of sample documents tested) and will include supporting documentation for all testing exceptions.

**Categorization and Escalation of Issues and Remediation**

When performing tests of operating effectiveness, management may find exceptions from prescribed control policies or procedures, as we do not expect controls to operate perfectly. In these instances, the nature and cause of the conditions must be investigated. Management is responsible for determining if a mitigating control compensates for the defective control and if that control is designed to achieve the same control objective. If the compensating control is appropriately designed, tests of operating effectiveness will then be performed on the compensating control.

When control testing exceptions are identified, the following steps will be taken:

*  The exception will be recorded in a repository of findings. The repository of findings will include the testing observation, control theme, potential implications, short-term and long-term action plans, potential impact of the weakness and identification of mitigating controls. Maintenance of the repository will be the responsibility of the project leader.
*  The exception will be brought to the attention of a “Resolution Team” which will include the process owner and appropriate management, including the project leader. The responsibility to determine whether a deficiency is a significant deficiency or a material weakness lies with management.
  
Deficiencies can range from inconsequential, to significant, to material weaknesses. In limited situations, there may be sufficient evidence to conclude that the error was an isolated incident. If this is the case, it may still be possible to conclude that the control is operating effectively. Management will assess whether deficiencies, either individually or in the aggregate, rise to the level of a significant deficiency or a material weakness. According to the [PCAOB](https://pcaobus.org/) Final Ruling, significant deficiencies and material weaknesses are defined as follows:

*  **Significant deficiency**: A control deficiency, or combination of control deficiencies, that adversely affects the company’s ability to initiate, authorize, record, process and report external financial data reliably in accordance with generally accepted accounting principles such that there is more than a remote likelihood that a misstatement of the company’s annual or interim financial statements that is more than inconsequential will not be prevented or detected.
*  **Material weakness**:A significant deficiency, or combination of significant deficiencies, that results in more than a remote likelihood that a material misstatement of the annual or interim financial statements will not be prevented or detected.

Deciding whether an internal control deficiency is a significant deficiency or material weakness requires both a detailed understanding by management of the relevant facts and circumstances, and a considerable amount of management judgment. Management will evaluate and formally document its assessment of the significance of a deficiency in internal control over financial reporting by determining the following:

*  	The likelihood that a deficiency, or combination of deficiencies, could result in a misstatement of an account balance or disclosure.
*  	The magnitude of potential misstatement resulting from the deficiency or deficiencies.

When corrective action is taken to remedy a control deficiency, the corrected control should be in place and operating for a sufficient period of time prior to the assertion date for management to evaluate the corrected control and conclude that the control is operating effectively as of the assertion date. In addition, management will allow sufficient time to test the operating effectiveness of the control. Management will provide a rationale as to why a significant deficiency was not corrected or did not preclude them from concluding that the internal controls over financial reporting were operating effectively.

| **Appendix A: Evaluation of COSO Control Components** | 
| :------: | 


`Control Environment`

The control environment sets the tone of an organization, influencing the control consciousness of its people. It is the foundation for all components of internal control, providing discipline and structure.
The control environment encompasses several control factors, including:

*  Integrity and Ethical Values
*  Commitment to Competence
*  Board of Directors or Audit Committee
*  Management’s Philosophy and Operating Style
*  Organizational Structure
*  Assignment of Authority and Responsibility
*  Human Resource Policies and Procedures

These controls can be broken down as either hard controls (audit committee, organizational structure, assignment of authority and responsibility, human resources policies and procedures) or soft, cultural controls (integrity and ethics, commitment to competence, management operating style). Analysis of the control environment includes consideration as to whether the hard controls are functioning effectively and if there appears to be a breakdown in the soft controls.

Effectively controlled entities strive to have competent people, instill an enterprise-wide attitude of integrity and control consciousness, and set a positive “tone at the top.” They establish appropriate policies and procedures, often including a written code of conduct, which foster shared values and teamwork in pursuit of the entity’s objectives.

`Risk Assessment`

Every institution faces a variety of risks from external and internal sources that must be assessed. A precondition to risk assessment is establishment of objectives, linked at different levels and internally consistent. Risk assessment is the identification and analysis of relevant risks to achievement of the objectives, forming a basis for determining how the risks should be managed. Because economic, industry, regulatory, and operating conditions will continue to change, mechanisms are needed to identify and deal with the special risks associated with change.
According to COSO, effective risk assessment requires:

*  Definition of established objectives at both the strategic (entity-wide) level and the process (activity) level. Objectives must be relevant, consistent, and compatible across levels. 
*  Identification of relevant internal and external risk factors that could impact achievement of the defined objectives.
*  Mechanisms in place to continually re-assess risk and react to changing environments.

`Control Activities`

Control activities are the policies and procedures that help ensure management directives are carried out. They help ensure that necessary actions are taken to address risks to achievement of the entity’s objectives. Control activities occur throughout the organization, at all levels and in all functions. 
Specific control activities and related control objectives should be documented. With respect to financial reporting, some generic control activities may include written policies and procedures, appropriate authorizations, adequate record keeping, management reviews, and asset safeguards. Control activities over financial reporting may also include or overlap with information system and operational controls. When evaluating control activities, management must consider:

*  Appropriate policies and procedures existence and are consistently applied.
*  Documented control activities are functioning as intended.

`Information and Communication`

Pertinent information should be identified, captured, processed, and communicated in a form and timeframe that enables the individuals to carry out their responsibilities. Information systems produce reports that contain operational, financial and compliance-related information and make it possible to run and control the business. They deal not only with internally generated data, but also information about external events, activities and conditions necessary for informed business decision-making and external reporting. Effective communication also must occur in a broader sense, flowing down, across and up the organization, as well as externally.
Management must consider the following control criteria for processing information:

*  Information is relevant – internal and external information is obtained relative to established objectives.
*  Information is provided in timely fashion to appropriate persons and in sufficient detail, such that they can carry out their responsibilities efficiently and effectively.

Communication of information is inherent in processing information; management must consider:

*  Effective communication to employees of roles and control responsibilities.
*  Established and effective channels of communication for employees to report suggested improvements or improprieties.
*  Adequacy of communication across processes.
*  Adequacy of communication to external parties such as customers or suppliers.
*  Management’s responsiveness to information communicated from external and internal sources.

`Monitoring`

Internal control systems need to be monitored – a process that assesses the quality of the system’s performance over time. This is accomplished through ongoing monitoring activities, separate evaluations, or a combination of the two.

Monitoring occurs through management and supervisory personnel assessing the quality of internal control systems over the ordinary course of operations. In the evaluation of the monitoring component, management must consider the following criteria:

*  Extent to which personnel obtain evidence as to the continuing effectiveness of internal control.
*  Periodic comparison of accounting system information to physical assets.
*  Management responsiveness to recommendations on strengthening internal controls.
*  Extent to which information and communication provide feedback to management as to the continuing effectiveness of controls.
*  Effectiveness of internal audit activities.



| **Appendix B: Testing Techniques** | 
| :------: | 


|` Testing Technique`| `Description`|
| ------ | ------ |
| Observation | Observe the performance of the control. |
| Inquiry| Ask a knowledgeable person for information about the operation of a control; evaluate and obtain evidence about the appropriateness of the follow-up actions.
Note that inquiry alone is not sufficient to provide evidence of operation effectiveness.| 
| Inspection | Review records or documents supporting and evidencing the operation of a control. | 
| Re-Performance | Re-perform the operation of a control to ascertain that it was performed correctly. | 
| Knowledge Assessment | Combine inquiry, inspection and re-performance techniques to test the individuals’ knowledge of a subject or competency to perform a control. | 
| Corroborative Inquiry | Corroborate the performance of a control through confirmation with other members of the organization. Corroboration is meant to confirm the validity and consistency of the application of the control as a test of operating effectiveness. | 
| System Query | Test that automated controls within an IT application are operating as expected.| 


| **Appendix C: Testing Steps by Control Category** | 
| :------: | 


`Authorization (Manual & System)`

*Definition*

Authorization includes:

*  	Approval of transactions executed in accordance with management’s general or specific policies and procedures.
*  	Access to assets and records in accordance with management’s general or specific policies and procedures.

*Points to consider when performing the listed test steps:*


*  	Select a sample of documents per the sampling guidelines.
*  	Review the sample for evidence of documented approval/authorization. Inspect documentation evidencing authorization (signatures or computer generated audit trail).
*  	Review to ensure approval/authorization is in compliance with the company Policy.
*  	Discuss the authorization process with those involved and understand if the process is appropriately controlled. Specifically, understand if the control has ever been circumvented and if these instances are appropriately controlled with appropriate parties notified.
*  	Discuss with Information Technology the mechanism for maintaining the appropriate schedule of authorizations within the system. Determine if terminated/resigned employees are removed from the list immediately on the effective dates.
*  	Determine if the existing authorization capabilities within the system are consistent with the Corporate Policy and if the process is appropriately controlled. If management review is performed consider using their test work to gain evidence related to effectiveness of the control.
*  	Inquire if the control has ever been circumvented and if these instances are appropriately controlled with appropriate parties notified. If the computer facilitates authorization, perform system query to determine whether the system access prohibits unauthorized processing.

`Exception/Edit Report Control`

*Definition*

Controls that fall into the exception/edit report category relate to when a report is generated to monitor something and followed-up on through to resolution. In most instances, the reports are focused on exceptions/edits as defined below, however in some instances it may just be a report. For example, if an aging report is generated by the system and followed up, the content does not necessarily represent edits or exceptions, but the control would fall into this category for test of design and test of effectiveness considerations.

*  Exception - a violation of a set standard
*  Edit - a change to a master file

In most instances the underlying data for an exception/edit report is to be tested.

*Points to consider when performing the listed test steps:*

*  	Select a sample of exception/edit reports per the sampling guideline.
*  	Inquire if operational or system changes have been made since the last review and if the exception/edit reports have been updated.
*  	Evaluate if the sample of reports have been generated with the frequency listed in the company policy. Inspect exception/edit reports considering:
     - Date of preparation (frequency)
     - Evidence of follow-up and corrective action
*  	Evaluate if the noted exception items on the selected reports are followed-up and resolved on a timely basis.
     - Assess the competency and knowledge of individuals responsible for follow up. Consider re-performance and use judgment to gather sufficient evidence to conclude.
     - Inspect evidence of follow up. Use judgment to gather sufficient evidence to conclude on whether follow up was appropriate.
     - Document name, dates, and summary of interview and follow up action taken.
     - Where this control is performed by a group/department also consider using the corroborative enquiry technique.
     - If the report is computer generated and key configured controls are used to compile the report, use the configuration/account mapping control category for consideration to ensure the compilation of the report and underlying configuration are appropriate.
*  	Determine if the unresolved items are tracked and reported to the next level.
*  	Evaluate if the exceptions on the selected reports have been documented and approved.

`Interface/Conversion Control`

*Definition*

*Data interfaces* – Data interfaces transfer specifically defined portions of information (data) between two computer systems, using either manual or automated means or a hybrid of both, and should ensure accuracy, completeness and integrity of the data being transferred. The job of a data interface is to transfer the data securely, once and only once, completely, accurately, with integrity, and to highlight any exceptions. Interfaces can be two-way (back and forth between two systems) or one-way (from one system to another), and can link new systems to old/Legacy systems or old/Legacy systems to new systems. 

*Data conversion* – Data conversion is the process of migrating data from a Legacy system (which may have old, duplicate, inaccurate, incomplete data, which reside in several places within the system) to a new system. To perform this process, the data needs to be cleansed, reviewed and synchronized prior to conversion (a critical step), then mapped (which may include parsing or other manipulation), reformatted, translated, consolidated and loaded into the new system (which may include a time lag or delay during which new data is created). Once the data has been converted and loaded into the new system, it must be maintained to ensure its completeness, existence, accuracy and integrity.

*Points to consider when performing the listed test steps:*


*  Select a sample of source data as per the sampling guidelines and trace to corresponding converted data/system in order to ensure the data was converted/interfaced accurately and completely.
  - Inspect documentation supporting initial design, function, implementation, including testing, and use judgement to gather sufficient evidence to conclude on whether it is appropriate.
  - Corroborate understanding of changes (whether made or not) with an IT and business owner and confirm that maintenance is performed on regular basis.
  - If changes were made in current year, inspect documentation of change ensuring procedures appropriate and authorized. Use judgment to gather sufficient evidence to conclude on whether it is appropriate. 
  - Perform procedures related to system access to change configuration as outlined in the system access control category.
  

*  Inquire about steps taken to ensure that data has been accurately and completely transferred. Determine if the following exist:
   - Items count or record count
   - Batch total or hash total
   - Integrity reports (e.g., out of balance report, unmatched sub ledger report)
   - Reconciliation of input to output

Select samples of interface controls identified from the above and re-perform the steps to determine if the interface is complete.

Inspect exception reports generated highlighting interface problems. Follow through on the resolution of an exception that occurred this year. This exception review may include on-line review exception messages evidence during observation procedures.


*  Inquire about the interface/conversion process. Determine if:
   - Manual intervention can occur during the interface process
   - Manual adjustments are identified and reviewed by a second person
   

*  Determine if processing/job schedules exist and are monitored. Review the processing/job schedule to determine if the following are defined:
  - Timeline when data entry can be performed
  - Timeline when data entry cannot be performed (e.g., data is locked)
  - Submission deadline for data feed
  - Timeline for job submission
  - Timeline for job run
  - Timeline for job completion
  

`Key Performance Indicator (KPI)`

*Definition*

Key performance indicators (“KPIs”) are the financial and non-financial quantitative measurements that are:

*  Collected by management, either continuously or periodically; and
*  Used by management to evaluate the extent of progress toward meeting management’s defined objectives.

Points to consider when performing the listed test steps:

*  Perform a trend analysis of the selected KPI & identify any unusual fluctuations.
*  Determine if the listed/expected KPIs are calculated appropriately and timely.
*  Determine if the listed/expected KPIs are reviewed and appropriate action is taken if the KPI’s fall outside the standard.
*  Inspect evidence of follow up. Use judgement to gather sufficient evidence to conclude on whether follow up was appropriate.
*  Assess the knowledge of individuals responsible for follow up. Consider re-performance and use judgement to gather sufficient evidence to conclude.
*  Document name, dates, and summary of interview and follow up action taken.
*  Inspect KPI documentation considering:
   - Date of preparation (timeliness)
   - Documented explanations for unusual fluctuations
   - KPI agrees to the related books/records
*  Evaluate appropriateness of established standards based on the company’s performance levels.

Additional considerations in gathering sample of KPIs:
Select only those KPIs that are both relevant to financial statement assertions and possess the following qualities:

*  Strong and valid;
*  Expected to produce reliable results; and
*  At an appropriate level of precision to detect significant misstatement (as defined by the Assessor). 

`Management Review`

*Definition*

Management review is the activity of a person different than the preparer, analyzing and performing oversight of activities performed. In many instances, it will be a manager reviewing the work of a subordinate. However, it is not limited to this. It may include co-workers reviewing each other’s work. Examples including internal audit activities, etc.

*Points to consider when performing the listed test steps:*

*  Select a sample of documents per the sampling guideline.
*  Review sample for documented evidence management review.
*  Assess the competency and knowledge of individuals responsible for follow up. Consider re-performance and use judgment to gather sufficient evidence to conclude.
*  Inspect evidence of follow up. Use judgment to gather sufficient evidence to conclude on whether follow up was appropriate.
*  Document name, dates, and summary of interview and follow up action taken.
*  Inspect supporting documentation evidencing review (e.g., signatures, board minutes, journal entries, etc.)
*  Determine if management has adequately documented their review, identified unusual items/trends/variances and that the issues noted have been appropriately researched and resolved timely.
    - If management review is performed in excess of another controls (e.g., a reconciliation the other control is reviewed by management) only one or the other controls may need to be tested (reconciliation or management review) depending on which is designed well and consider providing best evidence.

`Reconciliation`

*Definition*

Reconciliation is a control designed to verify that two items, such as computer systems, are consistent.

*Points to consider when performing the listed test steps:*

*  Select a sample of reconciliation documents per the sampling guidelines.
*  Review the sample for evidence of secondary review.
*  Inspect reconciliation(s) considering:
   - Agreement to the books/records reviewed
   - Timely date of preparation
   - Reconciliation balances
   - Significant unidentified reconciling items that might represent a balancing amount (e.g., captions such as unlabeled “differences” or “other”)?
*  Review to ensure the review process is in compliance with the company Policy.
   - If there is management review consider whether the initial preparation and follow up or reviews will provide the best evidence control and test appropriately. (Also see management review control category)
*  Verify balances to source systems (e.g., trace GL balance to GL).
*  In the sample selected determine if the reconciling items have been identified appropriately researched and resolved timely. 
*  Determine if segregation of duties is maintained between those processing/approving the transaction and those performing the reconciliation.

`Segregation of Duties`

*Definition*

The separation of duties and responsibilities of authorizing transactions, recording transactions and maintaining custody to prevent individuals from being in a position to both perpetrate and conceal an error or irregularity.

*Points to consider when performing the listed test steps:*

*  Understand by inquiry and observation whether there is adequate segregation of duties among those that authorize transactions, record transactions and maintain custody of assets.
   - Inspect the accounting policies and procedures manual noting that segregation of duties policies and procedures (specific to the area reviewed) are documented.
   - Use the corroborative enquiry technique to corroborate segregation.
*  Perform review of individuals and the tasks they perform, and review their user profile to determine if their system access allows the segregation of duties (i.e., the system access is limited to the tasks they perform and does not compromise the segregation of duties).

`System Access`

*Definition*

The ability that individual users or groups of users have within a computer information system processing environment, as determined and defined by access rights configured in the system. The access rights in the system agree to the access in practice.

*Points to consider when performing the listed test steps:*


*  Inquire about the maintenance process of grantinGLimiting access to system/transaction to appropriate personnel and determine if:
   - Terminated/resigned individuals are removed immediately from the system on the effective date; and
   - Segregation of duties is maintained in granting access to individuals.
*  Obtain listing of individuals with access to the system/function noted in the control description from the IT department.
   - Corroborate knowledge of individuals with respect to their understanding of their own access capabilities. Compare responses to managements understanding and/or established guidelines.
   - Perform system queries with respect to the following to corroborate understanding of access restrictions gained through discussions regarding:
     - Level of access;
     - Super user access; and 
     - Sensitive functions.
*  Select a sample of users from the list in accordance with the sampling guidance and obtain authorization/approval documents sent to IT for system access to review for evidence of appropriate approval/authorization.
*  	Review the listing and assess appropriateness of the user profile for the users identified. If inappropriate user profiles are identified, obtain explanation and request listing of transaction performed by the individuals to determine if unauthorized/inappropriate transactions exist.
*  	Determine if the user profiles and user access are subject to periodic review and if management has adequately documented their review and the issues of note have been appropriately researched and resolved. 
*  	Determine if security violation reports are generated & security breaches are followed up and investigated. Inspect exception reports generated highlighting exceptions to access restrictions. Follow through on the resolution of an exception that occurred this year and use judgment to obtain sufficient evidence to conclude on follow up (this may include on-line review of user profiles).

  
`System Configuration/Account Mapping ControlL`

*Definition*
System configuration and account mapping includes “switches” that can be set by turning them on or off to secure data against inappropriate processing, based on the organization’s business rules. If the switch is turned on, the checking can be customized for the particular organization to be very robust or very permissive. The more specific definition of each is as follows:

*  Configurable controls - specific “switches” that can be set by turning them on or off to secure data against inappropriate processing.
*  Account mapping - specific “switches” that can be set related to how a transaction is posted to the general ledger and then to the financial statements.

*Points to consider when performing the listed test steps:*


*  Inquire if financial, operational or system changes have been made since the last review and if configuration/account mapping has been updated.
   - Inspect documentation supporting initial design, function and implementation including testing and use judgment to gather sufficient evidence to conclude on whether it is appropriate. (This is to be completed in the first year of relying on the control and whenever there is a change in IT environment or system)
   - Corroborate understanding of changes (whether made or not) with an IT and business owner and confirm that maintenance is performed on regular basis.
   - If changes were made in current year, inspect documentation of change ensuring procedures are appropriate and authorized. Use judgment to gather sufficient evidence to conclude on whether it is appropriate. Perform procedures related to system access to change configuration as outlined in the system access control category.
*  	Select a sample of business rules and account mapping for review. Determine if the business rules and account mapping are valid with reference to current policy and procedures.
*  	For a sample of business rules and account mapping established, determine if system edit checks are set up to detect and prevent abnormalities. Perform the following:
	- Run systemic edit checks (e.g., posting limits, tolerance limits, validations and security settings) with a set of fictitious data to determine if these checks are in place and adequately functioning. 
	- Perform data mining against the database to determine if out of range data exists. Explain exceptions noted.
*  Obtain a listing of account mappings to determine if all accounts set up in the database have been mapped to a correct GL account. 
*  Inquire with the Information Systems Department about the user’s ability to change configuration and account mapping. Determine if change management procedures exist to track requests, changes made and perform testing prior to changes being implemented and released. Inquire if changes can be made and go undetected.


`Additional Consideration in Testing Configurable Controls or Account Mapping:`

Account mapping may be changeable in a “live” production environment by users. Mis-mapped accounts may not appear on the financial statements, or they may appear in an inappropriate manner such as in a suspense account or in an “opposite” category such as revenue to liability. An end user can circumvent configurable controls if the control is not appropriately set up to meet the company’s need and user access appropriate. For example, using the warning message “can continue” may not be as appropriate to meet the company’s needs as “cannot continue - transaction is “held/blocked.”

Configurable controls can override security control features. For example, not assigning “authorisation groups” to certain accounts, tables or programs can result in ineffective security. On the other hand, a configurable control can be set up but may not be effective unless the system access supports the control as configured (for example: a user with super user access can just change the configured control setting).
































