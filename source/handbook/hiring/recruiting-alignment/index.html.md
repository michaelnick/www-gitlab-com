---
layout: markdown_page
title: "Recruiting Alignment"
---

## Recruiter, Coordinator and Sourcer Alignment by Department

| Department                    | Recruiter       | Coordinator     |Sourcer     |
|--------------------------|-----------------|-----------------|-----------------|
| Board of Directors          | April Hoffbauer   | | |
| Executive          | April Hoffbauer    | Emily Mowry | Anastasia Pshegodskaya |
| Enterprise Sales, North America | Kelly Murdock   | Taharah Nix |Susan Hill        |
| Commercial Sales,	NA/EMEA | Marcus Carter  | Chantal Rollison |Susan Hill      |
| Commercial Sales, APAC | Simon Poon | Lea Hanopol | Viren Rana |
| Field Operations,	NA/EMEA/APAC | Kelly Murdock   | Taharah Nix |Susan Hill/Viren Rana - APAC        |
| Customer Success, EMEA | Debbie Harris  | Bernadett Gal |Kanwal Matharu |
| Customer Success, APAC | Simon Poon | Lea Hanopol | Viren Rana |
| Federal Sales, Customer Success, Marketing | Stephanie Kellert   | Shiloh Barry |Viren Rana |
| Marketing, North America | Steph Sarff   | Shiloh Barry |Viren Rana |
| Marketing, EMEA | Sean Delea   | Kike Adio |Viren Rana |
| G&A | Stephanie Garza   | Emily Mowry |Kanwal Matharu |
| Quality                   | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| UX                        | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Technical Writing         | Rupert Douglas                                          | Kike Adio        | Zsusanna Kovacs      |
| Support                   | Cyndi Walsh                                             | Chantal Rollison      | Alina Moise      |
| Security                  | Cyndi Walsh                                             | Chantal Rollison      | Zsusanna Kovacs      |
| Infrastructure            | Matt Allen                                              | Emily Mowry      | Chris Cruz |
| Development - Dev         | Catarina Ferreira                                       | Chantal Rollison        | Chris Cruz       |
| Development - Secure/Defend      | Liam McNally                                            | Lea Hanopol        | Alina Moise       |
| Development - Ops & CI/CD  | Eva Petreska                                            | Taharah Nix      | Zsuzsanna Kovacs      |
| Development - Enablement  | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Development - Growth      | Trust Ogor                                              | Bernadett Gal        | Alina Moise       |
| Engineering Leadership                | Liam McNally                                         | Lea Hanopol      |  Anastasia Pshegodskaya |
| Product Management  | Matt Allen                      | Emily Mowry |  Anastasia Pshegodskaya/Chris Cruz |
