---
layout: markdown_page
title: "UseCase driven GTM"
---

## On this page
{:.no_toc}


## UseCase driven Go to market

The [Customer UseCases](https://about.gitlab.com/handbook/use-cases/) page defines 8 customer use cases which are the typical problems that a customer selects Gitlab to solve. In product marketing we have a focus on GitLab stages, we also develop and curate collateral that aligns with a buyer's journey supporting the use cases.  

## Buyer's journey and typical collateral

As a prospect is determining that they have a specific problem to solve, they typically go through several stages of a [buyer's journey](/handbook/marketing/corporate-marketing/content/#content-stage--buyers-journey-definitions) which we hope ultimately leads them to selecting GitLab as their solution of choice.

|  **Awareness**  |  **Consideration**  |   **Decision / Purchase**  |
| --------- | ---------- | ----------- |
|Collateral and content designed to reach prospects in this stage of their journey should be focused on **educating them about the problems they are facing**, the business impact of their problems, and the reality that others are successfully solving the same problem |   In general, collateral and content designed to reach prospects in this stage of their journey should be focused on **positioning GitLab as a viable and compelling solution to their specific problem.**   |   In general, collateral and content designed to reach prospects in this stage of their journey should be focused on key information that a buyer needs to **justify GitLab as their chosen solution**.  |
| Typical **Awareness** Collateral  |  Typical **Consideration** Collateral |  Typical **Decision/Purchase** Collateral  |
|  **-** White papers describing the problem space <br> - Infographics illustrating the impact of the problem/challenge <br> - Analyst reports describing the problem/domain  <br> - Webinars focusing on the problem and how can be solved <br> - Troubleshooting guides to help overcome the problem  <br> - Analysis of public cases where the problem impacted an organization (i.e. outage, data loss, etc) | - White papers describing the innovative solutions to the problem <br> - Infographics illustrating the success and impact of solving the problem <br> - Analyst reports comparing different solutions in the market (MQ, Waves, etc) <br> - Webinars focusing on the success stories and how gitlab helped solve the problem <br> - Customer Case Studies, Videos, Logos, etc <br> - Solution Check Lists / Plans for how to solve the problem <br> - Comparisons between GitLab and other solutions  | - ROI calculators <br> - Implementation / Migration guides <br> - Getting Started info <br> - References (case studies) |
