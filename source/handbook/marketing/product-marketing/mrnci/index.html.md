---
layout: markdown_page
title: "Market Research and Customer Insight"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Market Research and Customer Insight at GitLab

  - [Analyst relations (AR)](/handbook/marketing/product-marketing/analyst-relations/)
  - [Customer reference program](/handbook/marketing/product-marketing/customer-reference-program/)

  - View the [Analyst Relations Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/940099?&label_name[]=Analyst%20Relations) and [AR-Cats Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/940116?&label_name[]=Analyst%20Relations) to see what's currently in progress.

### Which Market Research and Customer Insight team member should I contact?

  - Listed below are areas of responsibility within the Market Research and Customer Insight team:

    - [Kim](/company/team/#kimlock), Customer Reference Manager
    - [Joyce](/company/team/#joycetompsett), Analyst Relations Manager
    - [Colin](/company/team/#colinwfletcher), Manager, Market Research and Customer Insights
    - [Ashish](/company/team/#kuthiala), Director PMM

#### Customer Reference KPI's
  - Quantity of Case Studies published per month - 2 case studies published per month
