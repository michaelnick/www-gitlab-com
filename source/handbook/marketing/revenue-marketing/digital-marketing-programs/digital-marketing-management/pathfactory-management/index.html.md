---
layout: markdown_page
title: "Using PathFactory"
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Using PathFactory

PathFactory is our [content library](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/#content-library). Digital marketing program managers work with marketing program managers to organize and curate assets into tracks that are then disseminated for use in Marketo, `about.gitlab.com`, and other campaign-related channels. If you require a track for a campaign or marketing initiative, please use the [PathFactory request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/pathfactory_request.md) in the [Digital Marketing Programs repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs). If you have another PathFactory request unrelated to building a target track, please create an issue in the [Digital Marketing Programs repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs) and assign `@sdaily`.

### Rules of Engagement

| Team | DRI | Responsibilities |
| ------ | ------ | ------ |
| Digital Marketing Programs | `@sdaily` | Upload gated content |
| Digital Marketing Programs | `@sdaily` | Managing adding UTMs when activating in channels |
| Digital Marketing Programs | `@sdaily` | Web promoter strategy |
| Digital Marketing Programs | `@sdaily` | QA, dissemination, and management of links |
| Digital Marketing Programs | `@sdaily` | Removing expired assets |
| Digital Marketing Programs | `@sdaily` | PathFactory analytics |
| Digital Marketing Programs | `@mnguyen4` `@shanerice` | Testing landing page (form fill > PF > experience) |
| Marketing Programs | MPM | Creation of target tracks related to campaigns |
| Marketing Programs | MPM | Adding content to relevant tracks in intended order |
| Marketing Programs | MPM | Creation of recommended tracks |
| Marketing Programs | MPM | Form strategy for each track |
| Marketing Programs | MPM | Measuring success of workflow |
| Marketing Operations | `@nlarue` | QA of PathFactory scoring |
| Marketing Operations | `@nlarue` | QA of PathFactory scoring > MQL scoring |
| Marketing Operations | `@nlarue` | QA of data flow to SFDC |
| Marketing Operations | `@nlarue` `@sdaily` | QA of implementing links |
| Corporate Marketing | blog author | Upload blog post |
| Corporate Marketing | `@atflowers` | Upload YouTube videos |
| Field Marketing | FMM | Utilize links in library for follow-up emails |
| Product Marketing | PMM | Utilize links in library |

### User Roles

**Author**

* Change your password
* Build, edit, and publish Content Tracks
* Apply existing tags to content assets
* View all analytics pages within your organization’s PathFactory instance
* Download data in CSV files

**Note:** Authors can access the content tag configuration page to view all existing tags, but it will be in view-only mode and they will not have the option to create new tags or edit existing ones.

**Reporter**

* View all analytics pages within your organization’s PathFactory instance
* Download data in CSV files
* Change your password

## Managing PathFactory

### Uploading content to the library

* Follow the [blog style guide](/handbook/marketing/corporate-marketing/content/editorial-team/).
* Remove `| GitLab` from the public and internal title
* Public and internal title should always match for the sake of consistency and search within the platform.
* **Public Title:** the title of the asset that you would be comfortable with your prospects or customers seeing and makes sense to the world outside of your organization.
* If you need to make any notes about an asset, you can use the internal title (example: `[EXPIRES SOON] Name or title of asset`).
* Expiry dates are not functional so you must manually deactivate the content if it’s past the expiry date.
* **Internal title:** where you can apply your chosen naming conventions, and have the asset names include markers and abbreviations which are meaningful to you and your organization (example: expiry date for analyst reports).
* Provide a clear and concise description of the content if one does not exist (most times you can simply grab the subheading from the page).
* Leave the engagement score at a default of 20 seconds with a score of 1 (for now).
* Ensure language is set to English.
* Leave Business Units and External ID blank (for now).
* **For video assets of any kind:** Use the prefix `[Watch]` before the title.
* Make sure you have the most valuable version of the asset (blog post vs. case study or PDF).
* Make sure you have the most recent version of the asset.
* Before you upload content, please use the search to determine if it’s already been added.
* Update the custom URL slug to be descriptive of the content with no stop words (and, the, etc.).
  *  **Note:** If you change a custom URL slug that is part of a content track, this action can affect any links to this item that have been previously shared.

### How content track promoters work

* You can only use the `Header` feature with the `Sign Posts` and `Bottom Bar` promoters.
* The `Header` is used to add additional branding to your content track.
* The `Flow`, `Sign Posts`, and `Bottom Bar` cannot be used together. Choose 1 of the 3.
    * **Flow:** Scrollable content menu allows visitors to jump ahead in their Content Track, or simply use the Next buttons to move forward.
    * **Sign Posts:** Customizable Next and Previous buttons allow visitors to navigate through content. Used for more of a linear journey through the content.
    * **Bottom Bar:** Collapsible content menu along page bottom.
* The `End Promoter`, `Exit`, and `Inactivity` promoters can be used in conjunction with either the `Flow`, `Sign Posts`, or `Bottom Bar` promoters.
    * **End Promoter:** Opens final asset in a new tab. 
        * Available overrides:
            * Link
            * CTA Label
            * Delay (seconds)
    * **Exit:** Suggested content window appears when visitor tries to navigate away from the Content Track. 
        * Available overrides:
            * Headline
            * Message
            * Items to show (choose from assets within the current track)
            * Delay (seconds)
    * **Inactivity:** Message flashes on tab when left inactive.
        * Available overrides:
            * Inactive tab title
            * Delay (seconds)

### Content track management

* Ensure that the content track settings have the Meta Tag set to `No Index`
* Every track should include a main CTA in the sidebar if using the “Flow” promoter.
* **When a track is LIVE (being used):** change the target track title in use to `[LIVE] Name of track`.
* **When testing a track:**
  * Remove any extraneous `?` (there should only be one immediately after the end of the URL).
  * Watch for extra `&`.
  * Ping `@sdaily` to test and ensure the experience is working as intended
* **When adding a PathFactory URL to Marketo:**
  * Remove any extraneous `?` (there should only be one immediately after the end of the URL).
  * Watch for extra `&`.
  * Ping `@sdaily` to review the link before implementation for quality assurance purposes.
* Before you edit, add, or remove an asset from a track, please create an issue and assign `@sdaily` to ensure that any live links on `about.gitlab.com` are updated accordingly.
* If a piece of content is moving out of a track, please create an issue so links inside of nurture emails can be updated accordingly. Removing or changing an asset in a `[LIVE]` track can disrupt the user experience for the visitor and activate the `#all` track instead of the intended content track.
* To ensure proper tracking of an asset in PathFactory, it should be included within a content track and not shared with an individual link.
* Turn on the `Cookie Consent` before providing the approved content track link for live use.

#### Content track best practices

**Target Track**

* Curated content
* Known audience
* Personalized journey (email, website, targeted display)
* 5-7 pieces of content
* Use target in webinar reg and follow-up
* Use target to GUIDE

**Recommended tracks**

* Dynamic content sequence (it will automatically move content to the top of the track that is performing well)
* Anonymous audience
* Personalized journey (web, general display, social)
* Tracks the most popular journey (which pieces are being viewed, can be exported into a target track)
* Use recommend to DISCOVER

### Using Form Strategy

1. Content assessment 
    * Choose primary content asset 
    * Choose supporting content assets to group 
    * Upload all content assets to content library 
2. Build a content track 
    * Add assets to track 
    * Create new or use existing​languages​and appearances​settings 
    * Other things to consider: 
        * Ensure your sharing settings​are set to direct visitors to your organization’s main social pages 
3. Set up your form 
    * Add your external form to PathFactory 
    * Add the form using either the external url or custom html 
    * Add a capture tag​to your form to ensure de-anonymization 
    * Submit a test form fill to ensure the data is being sent to your MAP 
    * Add the form to your content track 
4. Replace the content download link with the content track’s URL

### Using PathFactory links

#### Appending UTMs to PathFactory Links

1. First check and see if there is a question mark `?` already existing in the PF link. Typically there is one. The only time it won't have a `?` is when you set a custom URL.
2. If there is a question mark `?`, first add an ampersand `&` at the end of the PF link, followed by the UTMs. 
    * For example:
        * PF Link: `https://learn.gitlab.com/c/10-strategies-to-red?x=53kuPb`
        * PF Link with UTMs: `https://learn.gitlab.com/c/10-strategies-to-red?x=53kuPb&utm_source=email&utm_campaign=test`
3. If there is no question mark `?`, first add a question mark `?` at the end of the PF link, followed by the UTMs.
    * PF Link: `https://learn.gitlab.com/c/10-strategies-to-red`
    * PF Link with UTMs: `https://learn.gitlab.com/c/10-strategies-to-red?utm_source=email&utm_campaign=test`

### Live target tracks

#### Awareness

* [AWS Digital Media Services Reddit](https://learn.gitlab.com/c/git-lab-aws-solution?x=6uMhCn&lb_email={{lead.Email Address}})
* [Developer Survey](https://learn.gitlab.com/c/2019-global-develope?x=Cmf3nD&lb_email={{lead.Email Address}})

#### MPM campaigns

* [CI/CD](https://learn.gitlab.com/c/gitlab-scaled-ci-cd-?x=RSo4AM&lb_email={{lead.Email Address}})
* [Deliver Better Products Faster](https://learn.gitlab.com/c/gitlab-concurrent-de?x=QnKq41&lb_email={{lead.Email Address}})
* [Increase Operational Efficiencies](https://learn.gitlab.com/c/201906-gitlab-forres?x=Tt__oC&lb_email={{lead.Email Address}})
* [Competitive Reboot](https://learn.gitlab.com/c/modernize-your-ci-cd?x=eK9OiC&lb_email={{lead.Email Address}})
* [Reduce Security and Compliance Risk](https://learn.gitlab.com/c/gitlab-seismic-shift?x=gsG52p&lb_email={{lead.Email Address}})

#### Pillar tracks

* [Agile delivery](https://learn.gitlab.com/c/201906-whitepaper-re?x=9TzWQ4&lb_email={{lead.Email Address}})
* [Cloud Native-Google and GitLab](https://learn.gitlab.com/c/what-is-cloud-native-2?x=q_7T7D&lb_email={{lead.Email Address}})
* [Kubernetes](https://learn.gitlab.com/c/what-are-microservic?x=YuHR1t&lb_email={{lead.Email Address}})
* [Public Sector](https://learn.gitlab.com/c/gitlab-capabilities--1?x=XOIXTl&lb_email={{lead.Email Address}})

If you want to make changes to any of the following tracks, please create an issue and tag `@sdaily`.

### Tracking content

| Topic | Use | Example |
| ------ | ------ | ------ | 
| AWS | Content that relates Amazon Web Services. Likely use cases are case studies where the customer uses GitLab + AWS and integration information & tutorials. | [How to set up multi-account AWS SAM deployments with GitLab CI/CD](/2019/02/04/multi-account-aws-sam-deployments-with-gitlab-ci/) |
| Agile delivery | Content that relates to the agile delivery process decision framework which emphasizes incremental and iterative planning. | [What is an Agile mindset?](/2019/06/13/agile-mindset/) |
| Agile software development | Content that relates to the agile software development methodology which emphasizes cross-functional collaboration, continual improvement, and early delivery | [How to use GitLab for Agile software development](/2018/03/05/gitlab-for-agile-software-development/) |
| Application modernization | Content that relates to the process of converting, refactoring, re-writing, or porting legacy systems to more modern programming and infrastructure. Content on this topic may cover cost/benefit of updating legacy systems, process, system, and culture changes, and toolstack comparisons. | [3 Strategies for implementing a microservices architecture](/2019/06/17/strategies-microservices-architecture/) |
| Automation | Content that relates to using technology to automate tasks. Likely use cases are how automation impacts productivity and workflows, feature highlights & tutorials, and case studies. | [How IT automation impacts developer productivity](/2019/05/30/it-automation-developer-productivity/) |
| Azure | Content that talks specifically about Microsoft Azure. Likely uses cases are tutorials on using GitLab + Azure cloud or competitive content. | [Competitive analysis page for Azure DevOps](/devops-tools/azure-devops-vs-gitlab.html)
| CI/CD | Content that covers continuous integration, continuous delivery, and continuous deployment. This content is likely to more technical, explaining tools, methods for implementation, tutorials, and technical use cases. | [A quick guide to CI/CD pipelines](/2019/07/12/guide-to-ci-cd-pipelines/) |
| Cloud computing | Content that relates to the practice of using a network of remote servers hosted on the Internet to store, manage, and process data. Likely uses cases are content discussing various cloud models (public, private, hybrid, and multicloud), integrations, and tutorials. Some customer case studies may be tagged with this label if the case study is *primarily* about GitLab enabling their cloud computing model. | [Top 5 cloud trends of 2018: What has happened and what’s next](/2018/08/02/top-five-cloud-trends/index.html) |
| Cloud native | Content that relates container-based environments. Specifically, technologies are used to develop applications built with services packaged in containers, deployed as microservices and managed on elastic infrastructure through agile DevOps processes and continuous delivery workflows. | [A Cloud Native Transformation](/webcast/cloud-native-transformation/) |
| Containers | Content that relates to using, running, maintaining, and building for containers. | [Running Containerized Applications on Modern Serverless Platforms](https://www.youtube.com/watch?v=S8R7sSePAXQ)
| DevOps | Content that relates to DevOps methods, process, culture, and tooling. [Keys to DevOps success with Gene Kim](https://www.youtube.com/watch?v=dbkj0qXQ22A)
| DevSecOps| Content that relates specifically to integrating and automating security into the software development lifecycle. Content that relates to cybersecurity should be tagged `security` and not `devsecops`.| [A seismic shift in application security](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf) |
| Digital transformation |  | 
| GKE | Content that is specifically about Google Kubernetes engine and Google Cloud Platform. Likely use cases are integrations, tutorials, and case studies | [Demo: Deploy to GKE from GitLab](https://www.youtube.com/watch?v=u3jFf3tTtMk)
| Git | Content that relates to implementing and using the distributed version contronl system, Git. | [Moving to Git](/resources/downloads/gitlab-moving-to-git-whitepaper.pdf) |
| Jenkins | Content that is specifically about Jenkins. Likely uses cases are integrations, competitive, comparisons, and case studies. | [3 Teams left Jenkins: Here's why](2019/07/23/three-teams-left-jenkins-heres-why/) |
| Kubernetes| Contnet that relates to implementing and using kubernetes. Likely use cases are cost/benefits, tutorials, and use cases. | [Kubernetes and the future of cloud native: We chat with Kelsey Hightower](/2019/05/13/kubernetes-chat-with-kelsey-hightower/) |
| Microservices |  |
| Multicloud |  |
| Open source |  |
| SCM |  |
| Security | Content that relates to cybersecurity and application security practices. | [When technology outpaces security compliance](/2019/06/10/when-technology-outpaces-security-compliance/)
| Single application |  |
| Software development |  |
| Toolchain | Content that relates to toolchain and stack management. | [How to manage your toolchain before it manages you](/resources/downloads/201906-gitlab-forrester-toolchain.pdf)
| VSM | Content that relates to the topic of value stream mapping and management. Topics that fall under this tag may include cycle time, cycle analytics, and software delivery strategies and metrics. | [The Forrester Value Stream Management Report](/analysts/forrester-vsm/index.html) |
| Workflow | Content that relates to understanding and implementing workflows throughout the software development lifecycle. Likely uses are content that explains a particular workflow or how to set up a workflow in GitLab. For example: how a workflow might change when a level of automation is introduced. | [Planning for success](/resources/downloads/gitlab-planning-for-success-whitepaper.pdf) |

## PathFactory Analytics

To request an analytics report on a content track, create an issue in the Digital Marketing programs repo and tag `@sdaily`.