---
layout: markdown_page
title: "BC.1.01 - Business Continuity Plan Control Guidance"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# BC.1.01 - Business Continuity Plan
 
## Control Statement
GitLab's business continuity plan is reviewed, approved by management and communicated to relevant team members biannually.
 
## Context
The review cycle for business continuity plans are designed to ensure all information in the plan is as up-to-date as possible. A business continuity plan is only effective if users can trust the accuracy of the information in the plan. The business continuity plan is meant to be a comprehensive runbook that can walk all GitLab team-members through exactly what their individual responsibilities are, in the event of a disruption to GitLab operations. This triggering event can be anything from a malicious breach of our systems to a global datacenter disruption.
 
## Scope
The business continuity plan is comprehensive by nature and will impact all GitLab stakeholders. The scope of GitLab Business Continuity Plan will cover:
* BC plan for gitlab.com
* BC plan for customers.gitlab.com (Azure)
* BC plan for license.gitlab.com (AWS)
* Processes and procedures that support business operations and above environments


## Ownership
* Business Operations owns this control.
* Infrastructure will provide implementation support for all of the above mentioned sites. (.com, cutomers, license)
 
## Guidance
Steps to come up with a comprehensive BC plan, can be categorized into the following seven steps:
* Identify the critical business functions
* Identify the critical systems & its dependencies
* Identify the risks to business
* Specify and confirm the data backup and recovery plans are working efficiently.
* Document the functions, procedures and key personnel, who should lead the effort and who are all the key players involved.
* Prepare a detailed communication plan
* Test, assess, learn and improve

Based on the above, GitLab business continuity plan will have team and departmental pieces that roll up into one comprehensive plan. Each team knows best, as to what steps are needed in the event of a disruption to operations. Hence this overall plan is really more of a collection of individual plans and the packaging of these individual plans together. The plan should include the following: 

* Business decided and approved, RTO (recovery time objective) and RPO (recovery point objective)
* This Plan has to be reviewed, approved and signed off by senior management
* Documentation on critical business requirements, including backup plans, business contingency and other related needs and logistics surrounding these plans. 
* High-level steps/procedures that addresses how to respond in the event of the most likely disaster scenarios. 
* Once we have a high-level plan we can push this out to teams and have them create team-level plans.
* Ensure backups are running and include an additional full local backup on all servers and data as part of the BCDR plan and test this annually.
* Line of Communication and role assignments at each step. Document this list and make it readily available in times of need.
* Vendor communication and service restoration plan of action steps.
* Run a DR test annually, to ensure that the plan is working efficiently. 
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Business Continuity Plan issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/774) . 
 
### Policy Reference
 
## Framework Mapping
* ISO
  * A.17.1.1
  * A.17.1.2
* SOC2 CC
  * CC7.5
  * CC9.1
* SOC2 Availability
  * A1.2
* PCI
  * 12.10.1
 