---
layout: markdown_page
title: "CS Top 10 List for Product"
---

# CS Top 10 List for Product
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

- [CS Top 10 List for Product](https://docs.google.com/document/d/1hFmbfrd_EIoXkiPAAHgEPhnMF7FML59X7IOa4IjM_0E/edit) *(Current)*
- [Account Engagement](/handbook/customer-success/tam/engagement/) 
- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [Account Triage](/handbook/customer-success/tam/triage/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Gemstones](/handbook/customer-success/tam/gemstones/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Customer Health Scores](/handbook/customer-success/tam/health-scores/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)

---

## What
One of the [product sensing mechanisms](/handbook/product/#sensing-mechanisms) is to collect feedback from Customer Success on the top issues customers are looking for in the product.  To aid in gathering this information, the TAM team meets quarterly to collect and filter this information to the Product team using the process below.

## Process

1. TAMs gather customers' most requested and urgent product requests
1. Quarterly, the TAM team will meet to go over the most requested features and gather all those issues into a document
1. Together, the TAM team comes up with the most popular and most needed 20-30 features
1. Through some voting mechism, the entire Customer Success team votes on the 20-30 features to make this quarter's "Top 10 List" (can be up to 12 issues with ties)
1. TAMs will give results of voting to Product by opening an issue on the [product issue board](https://gitlab.com/gitlab-com/Product/issues/new?issuable_template=CS-Top-10) and attaching them there.
1. TAM representative will attend the next Weekly Product meeting to present the results.
1. After that, Product will take over and follow the process noted in the [issue template](https://gitlab.com/gitlab-com/Product/blob/master/.gitlab/issue_templates/CS-Top-10.md)
