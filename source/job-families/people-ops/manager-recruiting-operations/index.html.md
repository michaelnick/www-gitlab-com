---
layout: job_family_page
title: "Manager, Recruiting Operations"
---

This role reports to the VP of Recruiting.

## Responsibilities

* Lead a collaborative remote based [candidate experience team](/job-families/people-ops/candidate-experience-specialist/) that can scale to the dynamic demands of a rapidly growing world-wide technology company
* Provide coaching to improve performance of team members and drive accountability
* Remove operational roadblocks by troubleshooting and enabling scalable solutions in collaboration with stakeholders
* Identify  process improvement opportunities and implement changes in a fast-paced environment, consistently documenting in the handbook for institutional knowledge
* Onboard, mentor, and grow the careers of all team members
* Leverage  data from our applicant tracking system and scheduling tool to help drive decisions
* Build trusted partnerships with recruiting leaders, People Operations team, Legal, IT, and Department Leaders  to collaborate and drive alignment
* Leverage our candidate facing coordination team to market our employer brand to showcase that GitLab is a world-class place to work and ensuring that everyone on the team is a champion of our brand
* Help define consistent data-driven hiring metrics and deliver on the goals
* Partner closely with People Operations team to ensure a white glove handoff

## Requirements

* Exceptional cross-functional communication and organization skills, and demonstrated experience in time-management and ability to influence
* A creative mindset to get things done effectively and efficiently
* Familiarity with an ATS
* BS/BA degree
* Leadership or performance management experience
* Ability and passion to lead, mentor, and support the candidate experience team
* Strong time management, communication and organizational skills
* A team player with excellent client management skills
