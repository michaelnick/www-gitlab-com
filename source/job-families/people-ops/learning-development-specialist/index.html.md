---
layout: job_family_page
title: "Learning & Development Partner"
---

The Learning & Development Partner will identify and design the right learning solutions to support GitLab's growth. They will have deep experience in assessing organizational needs and developing a variety of learning solutions to drive development and growth within the company. They will also build and scale initiatives that focuses on company culture, individual development, strengthening our leadership, and organizational learning.

## Responsibilities

- Partner with members of the People group and the leadership team to identify, develop, and deliver solutions that support the growth of our team members and GitLab's success
- Establish an internal learning and development roadmap for all GitLab managers and individual contributors
- Research, develop, and facilitate exciting and impactful asynchronous and synchronous L&D programs
- Iterate on existing materials and design and develop new L&D content, utilizing GitLab’s YouTube channel and handbook
- Collaborate with the Sales team to design and deliver orientation content that develops new hires' understanding of GitLab’s business and platform
- Select a learning platform for GitLab that incorporates content from the handbook and provides methods for tracking assessments and completion
- Coordinate an effective marketing strategy to promote internal training
- Create the course catalog which will include e-learning, instructor-led courses, self study (books and articles), and hands-on workshops, utilizing the GitLab handbook to ensure learning solutions are accessible to everyone
- Monitor L&D metrics and iterate upon programs and courses for continuous improvement
- Create and design the supporting course material for all development programs, both for instructor-led and e-learning
- Solicit and incorporate team member feedback into our course content and experience

## Requirements

- 5+ years experience in related work such as instructional design and developing learning content
- Track record of designing engaging and impactful development programs that improves individual, team, and company performance
- Exceptional written and interpersonal skills
- Experience developing self-service learning content, such as e-learning modules
- Experience designing and delivering webinars or synchronous online courses
- Strong logistical planning and organizational skills
- Team-orientated, engaging, and energetic
- Capable of working collaboratively across multiple departments
- Passionate about personal development, training, learning, and seeing individuals develop to their fullest potential
- The ability to thrive in a fast-paced environment
- You share our [values](/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Learning & Development Generalist

- Support the L&D Partner to identify, develop, and deliver programs that support the growth of our team members and GitLab
- Work with the L&D Partner to roll out an internal learning and development roadmap for all GitLab managers and individual contributors
- Research, develop, and facilitate exciting and impactful asynchronous and synchronous L&D programs
- Coordinate an effective marketing strategy to promote internal training
- Contribute to the course catalog which will include e-learning, instructor-led courses, and hands-on workshops, utilizing the GitLab handbook to ensure the trainings are accessible to everyone
- Own the L&D metrics and provide feedback to the L&D Partner to iterate upon programs and courses for continuous improvement
- Solicit and incorporate team member feedback into our course content and experience securely

## Hiring Process

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Recruiters
- Next, candidates will be invited to schedule a 30 minute interview with our Director of People Operations
- Next, the candidate will be invited to interview with the Chief People Officer or People Business Partner.
- After that, candidates will be required to prepare and present a short 15 minute training on giving feedback to the Director of People Operations 
- Finally, our CEO may choose to conduct a final interview
