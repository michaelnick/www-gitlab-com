---
layout: job_family_page
title: "Content Marketing"
---

You are highly creative, self-motivated, and love to write and create content. 
You have experience building and engaging audiences across a variety of channels 
and content formats, and employ empathy and respect for your audiences. You are 
able to put yourself in the shoes of others in order to understand their needs, 
pain points, preferences, and ambitions and translate that into multimedia experiences. 
Additionally, you are excited about experimentation and analyzing the results.

## Responsibilities

- Manage content production from start to finish
- Create content including blog articles, infographics, eBooks, web pages, white papers, webinars, and reports
- Tailor content to buyer personas and segments
- Work with subject matter experts as needed to gather source material and technical expertise
- Contribute to the strategy and planning of content marketing initiatives
- Develop a deep understanding of our corporate and product messaging 
- Translate messaging into engaging narratives and multimedia experiences
- Copywriting and copyediting as needed

## Requirements

- Excellent writer and researcher with a strong ability to grasp new concepts quickly
- Degree in marketing, journalism, communications or related field
- Strong communication skills without a fear of over communication
- Familiarity with the content pillar concept
- Extremely detail-oriented and organized, able to meet deadlines
- You share our [values](/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

## Levels 

### Content Marketing Associate 

- Execute content plan set forth my the Manager, Content Marketing
- Writes multiple blog articles per week
- Synthesize messaging and expertise from Product Marketing into accurate and compelling content
- Participate in pitching articles and creative content ideas
- Contribute results to content marketing initiatives and OKRs

#### Requirements

- 1-3 years experience in a content-related role
- Proven ability to write high-quality, long-form content on complex topics
- Able to coordinate with multiple stakeholders and perform in a fast-moving start-up environment
- Proven ability to work on multiple projects at at time

### Content Marketing Manager

- Propose and execute on a content plan 
- Contribute ideas and solutions beyond existing plans
- Write multiple blog articles per week
- Strategically translate messaging into accurate, targeted, and compelling content
- Makes decisions on, and serve as DRI for, content marketing projects and initiatives
- Independently manage projects from start to finish
- Influence content marketing initiatives and OKRs, and drives results

#### Requirements

- 3-5 years experience in a content-related role
- Enterprise software marketing experience
- Proven ability to research and write on technical topics independently
- Proven abliity to perform a content gap analysis and execute on a content plan independently

### Senior Content Marketing Manager

- Participate in the development of the content marketing strategy
- Independently develop and execute a content plan
- Expertly and independently communicate product messaging into accurate, targeted, and compelling content
- Collaborate with Product Marketing on messaging and positioning
- Write weekly thought leadership articles
- Propose, implement, or lead improvements to content marketing strategy, plan, and process
- Mentor other members of the content marketing team
- Entrust work to other members of the content marketing team as appropriate
- Propose new content marketing tactics, opportunities, and audiences
- Responsible for ideation of content marketing initiatives, OKRs, and reporting on results 

#### Requirements

- 5+ years in a content-related role 
- 3+ years of enterprise software marketing experience
- Proven experience writing for technical audiences on one or more of the following topics: Coding, DevOps, Cloud Computing, IT Infrastructure, Cyber Security, or Application Security 
- In-depth industry experience and knowledge in a specialty
- Proven ability to identify new audiences and opportunities, create a strategy, and execute the strategy

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Development (Dev)

The Dev specialty maps to the [Dev department](/handbook/product/categories/#dev-department) at GitLab. This specialty 
covers the Manage, Plan, and Create stages of the [DevOps lifecycle](/stages-devops-lifecycle/). As a Dev specialist,
you are responsible for covering topics that fall under these stages. For example: Workflow policies, project management, value stream management, code review, and source code management.

### Operations (Ops)

The Ops specialty maps to the [Ops department](/handbook/product/categories/#ops-department) at GitLab. This specialty 
covers the Verify, Package, Release, Configure, and Monitor stages of the [DevOps lifecycle](/stages-devops-lifecycle/). As an Ops specialist,
you are responsible for covering topics that fall under these stages. For example: Continuous integration, continuous delivery, application performance management, PaaS, serverless, and Kubernetes.

### Security (Sec)

The Security specialty maps to the [Sec department](/handbook/product/categories/#sec-department) at GitLab. This specialty 
covers the Secure and Defend stages of the [DevOps lifecycle](/stages-devops-lifecycle/). As a Security specialist,
you are responsible for covering topics that fall under these stages. For example: Application security, SAST, DAST, container security, open source security, and continuous security testing.

### Email

As an email content specialist, you are responsible for copywriting email newsletters, nurture sequences, advertisments, campaigns, and announcements. 
You collaborate closely with the marketing program managers to execute email campaigns, A/B test email copy performance, and ensure
all email communications are consistent with our brand voice and style. 

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Candidates will submit 2-3 writing samples
- Next, candidates will be invited to 3 45-minute interviews: first with our Manager of Content Marketing, then a team interview, and finally with a Senior Product Marketing Manager
- Candidates will then be invited to schedule a 45-minute interview with our CMO
- Finally, our CEO may choose to conduct a final interview
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Customer Content Manager

The Customer Content Manager will partner closely with [Reference Program Manager](https://about.gitlab.com/job-families/marketing/reference-program-manager/) to turn customer relationships into compelling content and be a champion of our customers’ success. You will be responsible for writing case studies, creating videos and customer stories on the blog, and repacking content for use across a variety of marketing channels. 

### Responsibilities

- Own GitLab’s customer content program including identifying GitLab’s strongest use cases, customer profiles, and ROI metrics
- Champion our customers’ success by amplifying their stories and positioning them as leaders in the industry
- Partner with GitLab’s Reference Program Manager to identify and track customer content opportunities
- Conduct customer interviews and translate technical conversations into engaging, ROI-driven case studies
- Partner with GitLab’s Digital Production Manager to create and scale customer video content
- Create and repackage customer content for a variety of channels including web, blog, video, and social 
- Update and improve [about.gitlab.com/customers](https://about.gitlab.com/customers/).  

### Requirements

- Experience in content marketing, journalism, or communications 
- A natural storyteller with excellent narration and writing skills
- Experience managing a high velocity case study program 
- Experience with video production 
- Able to coordinate across many teams and perform in a fast-moving startup environment
- Proven ability to be self-directed and work with minimal supervision
- Ability to travel 
- Outstanding written and verbal communication skills
- You share our [values](https://about.gitlab.com/handbook/values/) and work in accordance with those values

### Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team).

- [Screening call](https://about.gitlab.com/handbook/hiring/#screening-call) with our Global Recruiters
- Qualified candidates will be asked to submit 2-3 writing samples.
- 45 minute interview with our Reference Program Manager
- 45 minute interview with our Digital Production Manager
- 45 minute interview with our Manager, Content Marketing
- Interview with our Senior Director of Corporate Marketing 
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring).

## Manager, Content Marketing

Managers in the Content Marketing team are prolific and creative content strategists.
They define GitLab’s content marketing strategy, and manage a world-class team of content
creators to expand GitLab's influence, awareness, subscribers, and leads.
While they are strong writers, editors, and creatives, their time is spent supporting
and growing the skills of their team. They own the delivery and results of Content Marketing
deliverables and OKRs. They are focused on improving results, productivity, and
operational efficiency. They must also collaborate across departments to
accomplish collaborative goals.

### Responsibilities

- Execute GitLab's content pillar strategy set forth by the Global Content Lead
- Manage a team of content marketers 
- Collaborate with Product Marketing on positioning and messaging of content
- Collaborate with Digital Marketing & Marketing Programs on content placement and activation
- Perform regular content gap analysis to idendify areas of success, improvement, and opportunity
- Hire and manage a world class team of content marketers 
- Hold regular 1:1s with all members of the team

### Requirements

- Degree in marketing, journalism, communications or related field
- 3-5 years experience in content marketing or journalism at an enterprise software company or news outlet
- 2+ years experience managing a team of writers
- Experience defining the high-level strategy and creating content plans based on research and data
- A dual-minded approach: Highly creative and an excellent writer/editor but also process-driven, think scale, and rely on data to make decisions
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors
- Obsessive about content quality not quantity
- Regular reporting on content and channel performance
- You share our [values](/handbook/values), and work in accordance with those values
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
- BONUS: A passion and strong understanding of the industry and our mission

## Senior Manager, Content Marketing 

### Requirements 

- Degree in marketing, journalism, communications or related field.
- 5+ years experience in content marketing or journalism at an enterprise software company or news outlet.
- 2+ years experience managing a team of writers.
- Experience defining the high-level strategy and creating content plans based on research and data.
- A dual-minded approach: Highly creative and an excellent writer/editor but also process-driven, think scale, and rely on data to make decisions.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors.
- Obsessive about content quality not quantity.
- Regular reporting on content and channel performance.
- You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
- BONUS: A passion and strong understanding of the industry and our mission.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

- Screening call with recruiter
- Interview with Global Content Lead
- Interview with Director of Corporate Marketing (manager)
- Interview with Senior Manager, Corporate Communications
- Interview with CMO

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
