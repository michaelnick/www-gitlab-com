---
layout: markdown_page
title: "Category Strategy - Wiki"
---

- TOC
{:toc}

## Wiki

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting this category strategy page on Wiki in GitLab. This page belongs to the [Knowledge](/handbook/product/categories/#knowledge-group) group of the Create stage and is maintained by Kai Armstrong([E-Mail](mailto:karmstrong@gitlab.com)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=wiki) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=wiki) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Wiki, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

Share documentation and organization information with a built in wiki. Each project includes a Wiki rendered by Gollum, and backed by a Git repository.

### Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

We are aiming for real time WYSIWYG collaboration (similar to Google Docs) but stored in Markdown and accessible by Git. This should solve the problem of collaborative note taking, be highly approachable for non-technical users, but have the tremendous benefits of storing the content in a portable plain text format that can be cloned, viewed and edited locally (properties of Git).

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

**Next: [Simplify wiki editing](https://gitlab.com/groups/gitlab-org/-/epics/204)** - Currently wiki editing is inconsistent with every other page, both visually and from a workflow perspective. This makes wiki less approachable for users familiar with the rest of GitLab. We should fix these inconsistencies as a priority.

**After: [Improve wiki navigation](https://gitlab.com/groups/gitlab-org/-/epics/700)** - As wikis grow with more and more content, GitLab is not providing the tools necessary to make them easy to navigate and use. This is a problem that can be resolved with support for macros and a few other small improvements.

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

We currently most closely compete with **GitHub Wikis** but we should be competing with:

- [Confluence](https://www.atlassian.com/software/confluence)
- [Notion](https://www.notion.so/)
- Google Docs


### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

### Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Wiki navigation issues](https://gitlab.com/groups/gitlab-org/-/epics/700)

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Wiki navigation issues](https://gitlab.com/groups/gitlab-org/-/epics/700)

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

Due to the current state of Wikis they are not used internally.

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->

- [Live editing](https://gitlab.com/groups/gitlab-org/-/epics/820)
