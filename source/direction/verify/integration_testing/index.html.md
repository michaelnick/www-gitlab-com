---
layout: markdown_page
title: "Category Vision - Integration Testing"
---

- TOC
{:toc}

## Integration Testing

It's important to validate that the components of your system work together well. By performing integration tests as part of your CI pipeline, you help ensure quality in every build.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3AIntegration%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/TBD) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

TBD

## Maturity Plan

TBD

## Competitive Landscape

TBD

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

TBD

